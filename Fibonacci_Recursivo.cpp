#include <stdio.h>
#include <conio.h>
#include<locale.h>

int fibonacci(int num)
{
   if(num==1 || num==2)
       return 1;
   else
       return fibonacci(num-1) + fibonacci(num-2);
}

main()
{
   setlocale(LC_ALL,"Portuguese");
   int n,i;
   printf("Insira a quantidade de termos que voc� deseja na sequ�ncia de Fibonacci: ");
   scanf("%d", &n);
   printf("\nA sequ�ncia de Fibonacci �: \n");
   for(i=0; i<n; i++)
       printf("%d ", fibonacci(i+1));
   getch();
}

